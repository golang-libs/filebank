package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/generic-apps/filebank/internal/config"
	"gitlab.com/generic-apps/filebank/internal/service"
	"log"
	"net/http"
)

func main() {
	log.Println("Starting...")
	log.Println("Loading config...")
	config.LoadLocalConfig(config.Config)
	config.CheckValidity()
	cfg := config.Config
	log.Printf("Debug mode enabled: %v", cfg.DebugEnabled)

	r := mux.NewRouter()

	r.HandleFunc("/upload/{app}/{module}/{token}", service.UploadFile).Methods("POST")
	r.HandleFunc("/path/{app}/{module}/{token}", service.GetFile).Methods("GET")

	r.HandleFunc("/upload/{app}/{module}", service.UploadFiles).Methods("POST")
	r.HandleFunc("/path/{app}/{module}", service.GetFiles).Methods("GET")

	r.HandleFunc("/file/{app}/{module}/{token}", service.ServeFileData).Methods("GET")
	r.HandleFunc("/delete/{app}/{module}/{token}", service.DeleteFile).Methods("DELETE")

	log.Println("Ready to serve Omnissiah.")
	err := http.ListenAndServe(":"+config.Config.AppPort, r)
	if err != nil {
		log.Fatalf("Cannot listen to port %v: %v", cfg.AppPort, err)
	}
}
