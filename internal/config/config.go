package config

import (
	"encoding/json"
	"flag"
	"log"
	"os"
)

type Configuration struct {
	AppPort      string
	DebugEnabled bool
	MaxMemory    int64
	StorageRoot  string
	NotFoundPath string
}

var Config = &Configuration{}

// Проверяет требуемые поля для каждого микросервиса
func CheckValidity() {
	if Config.AppPort == "" ||
		Config.StorageRoot == "" ||
		Config.MaxMemory == 0 {
		panic("config inconsistent")
	}
}

func LoadLocalConfig(config interface{}) {
	var path string
	flag.StringVar(&path, "config", "./config/conf.json", "Config path")
	flag.Parse()

	LoadConfig(path, config)
}

// LoadConfig needed for external things, like a tests, when we want to specify the config path manually.
func LoadConfig(path string, config interface{}) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalf("Can not open the config file: %s(%v)", path, err)
	}

	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatal("Error decoding config:", err)
	}
}
