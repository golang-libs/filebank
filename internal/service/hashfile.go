package service

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/generic-apps/filebank/internal/config"
	"gitlab.com/generic-apps/filebank/internal/model/requests"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

func UploadFiles(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]

	if module == "" || app == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err := r.ParseMultipartForm(config.Config.MaxMemory << 20)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(r.MultipartForm.File) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	req := make(map[string][]string, 0)
	err = json.Unmarshal([]byte(r.MultipartForm.Value["data"][0]), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if len(r.MultipartForm.File) != len(req) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	for filetype, files := range r.MultipartForm.File {
		log.Printf("===Uploaded filetype: %+v===\n", filetype)
		for i, file := range files {
			log.Printf("Uploaded File: %+v\n", file.Filename)
			log.Printf("File Size: %+v\n", file.Size)
			log.Printf("MIME Header: %+v\n", file.Header)

			data, err := file.Open()
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			fileBytes, err := ioutil.ReadAll(data)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			originalName := file.Filename
			filename := file.Filename
			filepath := getFilepath(app, module, req[filetype][i])
			if _, err := os.Stat(filepath + filename); os.IsExist(err) {
				for i := 2; i < 9999; i++ {
					if _, err := os.Stat(filepath + filename); os.IsNotExist(err) {
						filename = strconv.Itoa(i) + "_" + originalName
						break
					}
				}
			}

			err = os.MkdirAll(filepath, os.ModePerm)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			err = ioutil.WriteFile(filepath+filename, fileBytes, os.ModePerm)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	}
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]
	token := vars["token"]

	if token == "" || module == "" || app == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err := r.ParseMultipartForm(config.Config.MaxMemory << 20)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(r.MultipartForm.File) > 1 || len(r.MultipartForm.File) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	for filetype, files := range r.MultipartForm.File {
		log.Printf("===Uploaded filetype: %+v===\n", filetype)
		if len(files) > 1 || len(files) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		file := files[0]
		log.Printf("Uploaded File: %+v\n", file.Filename)
		log.Printf("File Size: %+v\n", file.Size)
		log.Printf("MIME Header: %+v\n", file.Header)

		data, err := file.Open()
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		fileBytes, err := ioutil.ReadAll(data)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		originalName := file.Filename
		filename := file.Filename
		filepath := getFilepath(app, module, token)
		if _, err := os.Stat(filepath + filename); os.IsExist(err) {
			for i := 2; i < 9999; i++ {
				if _, err := os.Stat(filepath + filename); os.IsNotExist(err) {
					filename = strconv.Itoa(i) + "_" + originalName
					break
				}
			}
		}

		err = os.MkdirAll(filepath, os.ModePerm)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = ioutil.WriteFile(filepath+filename, fileBytes, os.ModePerm)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

func getFilepath(app, module, token string) string {
	return fmt.Sprintf("%s/%s/%s/%s/", config.Config.StorageRoot, app, module, token)
}

func getWebFilepath(app, module, token string) string {
	return fmt.Sprintf("%s/%s/%s", app, module, token)
}

func getFiledata(app, module, token string) (int, string, string, error) {
	folder := getFilepath(app, module, token)
	if _, err := os.Stat(folder); os.IsNotExist(err) {
		return http.StatusNotFound, "", "", err
	}

	files, err := ioutil.ReadDir(folder)
	if err != nil {
		return http.StatusInternalServerError, "", "", err
	}

	for _, file := range files {
		if !file.IsDir() {
			return http.StatusOK, getWebFilepath(app, module, token), file.Name(), nil
		}
	}
	return http.StatusNotFound, "", "", nil
}

func GetFile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]
	token := vars["token"]

	code, path, name, err := getFiledata(app, module, token)
	if err != nil {
		log.Println(err)
		w.WriteHeader(code)
		return
	}

	filepath := struct {
		Token string            `json:"token"`
		Data  requests.Filedata `json:"data"`
	}{
		Token: token,
		Data:  requests.Filedata{Filename: name, Filepath: path},
	}
	resultJSON, err := json.Marshal(filepath)
	_, err = w.Write(resultJSON)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func ServeFileData(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]
	token := vars["token"]

	code, path, name, err := getFiledata(app, module, token)
	if err != nil {
		log.Println(err)
		w.WriteHeader(code)
		return
	}

	http.ServeFile(w, r, config.Config.StorageRoot+"/"+path+"/"+name)
}

func GetFiles(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]

	tokens, ok := r.URL.Query()["key"]
	if !ok || len(tokens[0]) < 1 {
		return
	}

	res := make(map[string]requests.Filedata)
	for _, token := range tokens {
		_, path, name, err := getFiledata(app, module, token)
		if err != nil {
			log.Println(err)
			path = config.Config.NotFoundPath
			continue
		}
		res[token] = requests.Filedata{Filename: name, Filepath: path}
	}
	resultJSON, err := json.Marshal(res)
	_, err = w.Write([]byte(resultJSON))
	if err != nil {
		fmt.Println(err)
	}
}

func DeleteFile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	app := vars["app"]
	module := vars["module"]
	token := vars["token"]

	folder := getFilepath(app, module, token)
	if _, err := os.Stat(folder); os.IsNotExist(err) {
		return
	}

	err := os.RemoveAll(folder)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
