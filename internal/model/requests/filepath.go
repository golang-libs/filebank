package requests

type Filedata struct {
	Filepath string `json:"filepath"`
	Filename string `json:"filename"`
}
