package middleware

import (
	"net/http"
)

type ErrorResponse struct {
	Message string `json:"message"`
}

//
//func formErrorResponse(e error, customCode int, customText string) (int, []byte) {
//	code := http.StatusInternalServerError
//	text := "Неизвестная ошибка"
//
//	// Выставлять ОК при ошибке нельзя!
//	if customCode != 0 && !isOKCode(customCode) {
//		code = customCode
//	}
//
//	if customText != "" {
//		text = customText
//	}
//	// Нам нужен реальный текст для дебага
//	if config.Config.DebugEnabled {
//		// что бы не проскакивали nil
//		c := errors.Cause(e)
//		if c != nil {
//			text = c.Error()
//		}
//	}
//
//	jsonErr := ErrorResponse{Message: text}
//	responseBytes, _ := json.Marshal(&jsonErr)
//	return code, responseBytes
//}
//
//func ErrHandler() http.Handler {
//	return func(ps ) http.Handler {
//		return func(r, w) {
//			defer func() {
//				if r := recover(); r != nil {
//					err := mosk.PanicToError(r)
//					sendErr := logging.SendErrorToLog(err)
//					if sendErr != nil {
//						log.Printf("ID: %s\n Error: %v\n", responseData.GetRequestId(), errors.Cause(err))
//						log.Printf("Sending to log failed with error: %v\n", errors.Cause(sendErr))
//					}
//					maskedCode, b := formErrorResponse(err, responseData.GetCode(), responseData.GetMessage())
//					responseData.SetCode(maskedCode)
//					responseData.AddHeaders(map[string]string{"Content-Type": "application/json"})
//					responseData.SetBody(b)
//				}
//			}()
//
//			ps(reqData, responseData)
//
//			maskedCode := http.StatusOK
//			var b []byte
//			err := responseData.GetError()
//			if err != nil || !isOKCode(responseData.GetCode()) {
//				maskedCode, b = formErrorResponse(err, responseData.GetCode(), responseData.GetMessage())
//				responseData.SetBody(b)
//
//				if err != nil {
//					sendErr := logging.SendErrorToLog(err)
//					if sendErr != nil {
//						log.Printf("ID: %s\n Error: %v\n", responseData.GetRequestId(), errors.Cause(err))
//						log.Printf("Sending to log failed with error: %v\n", errors.Cause(sendErr))
//					}
//				} //else {
//				//log.Printf("ID: %s\n Event: %s\n Message: %s\n Context: %s\n", responseData.GetRequestId(), "",
//				//	responseData.GetMessage(), "")
//				//}
//			}
//			responseData.SetCode(maskedCode)
//		}
//		}
//	}
//}

func isOKCode(code int) bool {
	// Либо задан НЕ ОК код, либо код вообще не задан
	return (code < http.StatusMultipleChoices && code >= http.StatusOK) || code == 0
}
